<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Enums\Task\TaskStatus;
use App\Http\Filters\TaskFilter;
use App\Models\Task;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class TaskRepository
{
    public function firstOrCreate(array $data): Task
    {
        return Task::firstOrCreate($data)->refresh();
    }

    public function update(array $data, Task $task): Task
    {
        $task->update($data);

        return $task;
    }

    public function delete(Task $task): void
    {
        $task->delete();
    }

    public function hasUncompletedSubtasks(Task $task): bool
    {
        return $task->subtasks()->where('status', TaskStatus::TODO)->exists();
    }

    public function getList(array $filter): Collection
    {
        return Auth::user()->tasks()
            ->where('parent_task_id')
            ->filter(new TaskFilter(array_filter($filter)))
            ->get();
    }

    public function find(int $id): ?Task
    {
        return Task::find($id);
    }


}
