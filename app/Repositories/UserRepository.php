<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Models\User;

class UserRepository
{
    public function create(array $data): User
    {
        $user = new User($data);

        $user->save();

        return $user;
    }
    
    public function getUserByEmail(string $email): ?User
    {
        return User::where('email', $email)->first();
    }
}
