<?php

declare(strict_types=1);

namespace App\Enums\Task;

enum TaskStatus: int
{
    case TODO = 1;
    case DONE = 2;


    public function toReadable(): string
    {
        return match ($this) {
            self::TODO => 'todo',
            self::DONE => 'done',
        };
    }
}
