<?php

declare(strict_types=1);

namespace App\Exceptions\Task;

use Exception;
use Throwable;

class CompletedTaskDeleteException extends Exception
{
    public function __construct(
        string $message = 'Cannot delete a completed task.',
        int $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
