<?php

declare(strict_types=1);

namespace App\Exceptions\Task;

use Exception;
use Throwable;

class CreateSubtaskOfCompletedTaskException extends Exception
{
    public function __construct(
        string $message = 'Cannot create a subtask of completed parent task.',
        int $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
