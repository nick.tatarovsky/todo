<?php

declare(strict_types=1);

namespace App\Exceptions\Task;

use Exception;
use Throwable;

class HasUncompletedSubtasksException extends Exception
{
    public function __construct(
        string $message = 'Cannot complete a task with uncompleted subtasks.',
        int $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
