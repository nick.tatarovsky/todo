<?php

declare(strict_types=1);

namespace App\Models;

use App\Http\Filters\TaskFilter;
use App\Models\Traits\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Task extends Model
{
    use Filterable;

    /**
     * @var string
     */
    protected $table = 'tasks';

    /**
     * @var string[]
     */
    protected $fillable = [
        'user_id', 'parent_task_id', 'status', 'priority', 'title', 'description', 'completed_at'
    ];

    /**
     * @var string[]
     */
    protected $dates = [
        'completed_at',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function subtasks(array $filter = []): HasMany
    {
        return $this->hasMany(self::class, 'parent_task_id')
            ->filter(new TaskFilter(array_filter($filter)));
    }
}
