<?php

declare(strict_types=1);

namespace App\Http\Requests\Task;

use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class StoreTaskRequest extends BaseTaskRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'parent_task_id' => 'nullable|int|exists:tasks,id',
            'title' => [
                'required',
                'string',
                'max:255',
                Rule::unique('tasks')->where(function ($query) {
                    return $query->where('user_id', Auth::id());
                }),
            ],
        ]);
    }
}
