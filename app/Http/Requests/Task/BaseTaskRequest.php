<?php

declare(strict_types=1);

namespace App\Http\Requests\Task;

use App\Enums\Task\TaskPriority;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

abstract class BaseTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'description' => 'nullable|string|max:255',
            'priority' => [
                'nullable',
                'int',
                new Enum(TaskPriority::class)
            ],
        ];
    }
}
