<?php

declare(strict_types=1);

namespace App\Http\Requests\Task;

use App\Enums\Task\TaskPriority;
use App\Models\Task;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class FilterTaskRequest extends BaseTaskRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'title' => 'nullable|string|max:255',
            'status' => ['nullable', 'int', new Enum(TaskPriority::class)],
            'priority_from' => ['nullable', 'int', new Enum(TaskPriority::class)],
            'priority_to' => ['nullable', 'int', new Enum(TaskPriority::class)],
            'sort_by' => 'nullable|string'
        ];
    }
}
