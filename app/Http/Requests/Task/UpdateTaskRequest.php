<?php

declare(strict_types=1);

namespace App\Http\Requests\Task;

use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdateTaskRequest extends BaseTaskRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'title' => [
                'required',
                'string',
                'max:255',
                Rule::unique('tasks')->ignore($this->task)->where(function ($query) {
                    return $query->where('user_id', Auth::id());
                }),
            ],
        ]);
    }
}
