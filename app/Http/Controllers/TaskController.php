<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Exceptions\Task\CompletedTaskDeleteException;
use App\Exceptions\Task\HasUncompletedSubtasksException;
use App\Http\Requests\Task\FilterTaskRequest;
use App\Http\Requests\Task\StoreTaskRequest;
use App\Http\Requests\Task\UpdateTaskRequest;
use App\Http\Resources\Task\TaskResource;
use App\Http\Resources\Task\TasksListResource;
use App\Models\Task;
use App\Services\TaskService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class TaskController extends Controller
{
    public function __construct(
        private readonly TaskService $taskService
    ) {
        //
    }

    public function store(StoreTaskRequest $request): JsonResponse
    {
        try {
            $task = $this->taskService->store($request->validated());
            return TaskResource::make($task)->response()->setStatusCode(201);
        } catch (CompletedTaskDeleteException $exception) {
            return getErrors([$exception->getMessage()], 403);
        }
    }

    public function index(FilterTaskRequest $request): AnonymousResourceCollection
    {
        return TasksListResource::collection($this->taskService->getList($request->validated()));
    }

    public function update(UpdateTaskRequest $request, Task $task): TaskResource
    {
        $this->authorize('update', $task);

        $this->taskService->update($request->validated(), $task);

        return TaskResource::make($task);
    }

    public function destroy(Task $task): JsonResponse
    {
        $this->authorize('delete', $task);

        try {
            $this->taskService->delete($task);
            return getSuccessResponse();
        } catch (CompletedTaskDeleteException $exception) {
            return getErrors([$exception->getMessage()], 403);
        }
    }

    public function complete(Task $task): TaskResource|JsonResponse
    {

        $this->authorize('update', $task);

        try {
            $task = $this->taskService->complete($task);
            return TaskResource::make($task);
        } catch (HasUncompletedSubtasksException $exception) {
            return getErrors([$exception->getMessage()], 403);
        }
    }
}
