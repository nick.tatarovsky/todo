<?php

declare(strict_types=1);

namespace App\Http\Resources\Task;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TasksListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'parent_task_id' => $this->parent_task_id,
            'status' => $this->status,
            'priority' => $this->priority,
            'title' => $this->title,
            'description' => $this->description,
            'completed_at' => $this->completed_at,
            'subtasks' => $this->getSubtasks()
        ];
    }

    private function getSubtasks(): array
    {
        return self::collection($this->subtasks)->resolve();
    }
}
