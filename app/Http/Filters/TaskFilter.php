<?php

declare(strict_types=1);

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

class TaskFilter extends AbstractFilter
{
    private const TITLE = 'title';
    private const STATUS = 'status';
    private const PRIORITY_FROM = 'priority_from';
    private const PRIORITY_TO = 'priority_to';
    private const SORT_BY = 'sort_by';

    protected function getCallbacks(): array
    {
        return [
              self::TITLE => [$this, 'title'],
              self::STATUS => [$this, 'status'],
              self::PRIORITY_FROM => [$this, 'priorityFrom'],
              self::PRIORITY_TO => [$this, 'priorityTo'],
              self::SORT_BY => [$this, 'sortBy'],
        ];
    }

    public function title(Builder $builder, string $title): void
    {
        $builder->where('title', 'like', "%$title%");
    }

    public function status(Builder $builder, int $status): void
    {
        $builder->where('status', $status);
    }

    public function priorityFrom(Builder $builder, int $priority): void
    {
        $builder->where('priority', '>=', $priority);
    }

    public function priorityTo(Builder $builder, int $priority): void
    {
        $builder->where('priority', '<=', $priority);
    }

    public function sortBy(Builder $builder, string $value): void
    {
        $sortDirection = Str::startsWith($value, '-') ? 'desc' : 'asc';

        match (ltrim($value, '-')) {
            'created_at' => $builder->orderBy('created_at', $sortDirection),
            'completed_at' => $builder->orderBy('completed_at', $sortDirection),
            'priority' => $builder->orderBy('priority', $sortDirection),
            default => $builder->orderByDesc('id'),
        };
    }
}
