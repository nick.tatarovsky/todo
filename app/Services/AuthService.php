<?php

declare(strict_types=1);

namespace App\Services;

use App\Enums\Mailchimp\MailchimpUserGroup;
use App\Events\UserGroupUpdatedEvent;
use App\Events\UserRegisteredEvent;
use App\Http\Requests\Auth\LoginRequest;
use App\Repositories\UserRepository;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuthService
{
    public function __construct(
        private readonly UserRepository $userRepository,
    ) {
        //
    }

    /**
     * @throws HttpResponseException
     */
    public function login(array $credentials)
    {
        if (! Auth::attempt(Arr::only($credentials, ['email', 'password']))) {
            throw new HttpResponseException(getErrors([
                'password' => ['Email or password is incorrect']
            ], 401));
        }

        $user = $this->userRepository->getUserByEmail(Arr::get($credentials, 'email'));

        return $user->createToken('auth-token')->plainTextToken;
    }

    /**
     * @throws HttpResponseException
     */
    public function register(array $data)
    {
        DB::beginTransaction();
        try {
            $user = DB::transaction(function () use ($data) {
                return $this->userRepository->create($data);
            });

            DB::commit();

            return $user;
        } catch (Throwable $e) {
            DB::rollBack();
            report($e);
            abort(500);
        }
    }
}
