<?php

declare(strict_types=1);

namespace App\Services;

use App\Enums\Task\TaskStatus;
use App\Exceptions\Task\CompletedTaskDeleteException;
use App\Exceptions\Task\CreateSubtaskOfCompletedTaskException;
use App\Exceptions\Task\HasUncompletedSubtasksException;
use App\Models\Task;
use App\Repositories\TaskRepository;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class TaskService
{
    public function __construct(
        private readonly TaskRepository $taskRepository,
    ) {
        //
    }

    public function store(array $data): Task
    {
        $parentTask = $this->taskRepository->find(Arr::get($data, 'parent_task_id'));

        if ($parentTask && $this->taskRepository->hasUncompletedSubtasks($parentTask)) {
            throw new CreateSubtaskOfCompletedTaskException();
        }

        return $this->taskRepository->firstOrCreate(array_merge($data, [
            'user_id' => Auth::id(),
        ]));
    }

    public function update(array $data, Task $task): Task
    {
        return $this->taskRepository->update($data, $task);
    }

    public function getList(array $filter): Collection
    {
        $tasks = $this->taskRepository->getList($filter);

        return ($this->addFilteredSubtasks($tasks, $filter));
    }

    private function addFilteredSubtasks($tasks, $filter)
    {
        $tasks->each(function ($task) use ($filter) {
            $task->subtasks = $this->getFilteredSubtasks($task, $filter);
        });

        return $tasks;
    }

    private function getFilteredSubtasks($task, $filter)
    {
        return $task->subtasks($filter)->get()->each(function ($subtask) use ($filter) {
            $subtask->subtasks = $this->getFilteredSubtasks($subtask, $filter);
        });
    }

    public function delete(Task $task): void
    {
        if ($task->status === TaskStatus::DONE->value) {
            throw new CompletedTaskDeleteException();
        }

        $this->taskRepository->delete($task);
    }

    public function complete(Task $task): ?Task
    {
        if ($this->taskRepository->hasUncompletedSubtasks($task)) {
            throw new HasUncompletedSubtasksException();
        }

        return $this->taskRepository->update([
            'completed_at' => now(),
            'status' => TaskStatus::DONE
        ], $task);
    }
}
