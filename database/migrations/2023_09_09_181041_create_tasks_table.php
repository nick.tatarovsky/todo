<?php

declare(strict_types=1);

use App\Enums\Task\TaskPriority;
use App\Enums\Task\TaskStatus;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    private const TABLE = 'tasks';

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create(self::TABLE, function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained();
            $table->unsignedBigInteger('parent_task_id')->nullable();
            $table->unsignedTinyInteger('status')->default(TaskStatus::TODO->value);
            $table->unsignedTinyInteger('priority')->default(TaskPriority::MEDIUM->value);
            $table->string('title', 255);
            $table->text('description')->nullable();
            $table->dateTime('completed_at')->nullable();
            $table->timestamps();

            $table->foreign('parent_task_id')
                ->references('id')
                ->on(self::TABLE)
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('todos');
    }
};
