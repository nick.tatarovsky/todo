# ToDo List
## Вимоги
Docker v3

## Встановлення
1. Клонуйте віддалений репізоторій на локальну машину
```bash
git clone https://gitlab.com/nick.tatarovsky/todo.git
```
2. Перейдіть до кореневої теки проекту
```bash
cd todo
```
3. Створіть файл .env, скопіювавши файл .env.example
```bash
cp .env.example .env
```
4. Зберіть та запустіть контейнери Docker
```bash
docker-compose up -d
```
5. Встановіть залежності проекту
```bash
docker-compose exec app composer install
```
6. Сгенеруйте ключ застосунку
```bash
docker-compose exec app php artisan key:generate
```

## База даних
За замовченням використовується MySQL в контейнері
```bash
DB_CONNECTION=mysql
DB_HOST=mysql
DB_PORT=3306
DB_DATABASE=main
DB_USERNAME=root
DB_PASSWORD=root
```
Для зручності можна змітини на SQLite в .env
```bash
DB_CONNECTION=sqlite
```
Після налаштування підключення до бази даних скиньте налаштування оточення
```bash
docker-compose exec app php artisan config:clear
```
Запустіть міграції, погодьтесь на створення нової бази даних
```bash
docker-compose exec app php artisan migrate
```

## Postman колекція
https://drive.google.com/file/d/1Xq5az9zt2X2vfV2YUyiJKIbTSU7x-ey_/view?usp=sharing

